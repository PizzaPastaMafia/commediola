#Nel codice s'immerge il testo divino,
#Commedia.txt, tra i righi intrecciuti,
#Svelando il lungo sentiero al suo destino.

#Il programma scrutando, con occhi astuti,
#Indaga le righe, misura il respiro,
#Nella sua lunghezza svela gli arguti.

def tuple_compare(tup):
    """
    Input: 2-tuple of the form (anything, line)
    Output: Length of line with trailing newline stripped.
    """
    unused_anything, line = tup
    return len(line.rstrip('\n'))

with open('commedia1.txt', encoding='utf-8') as fin:
    biggest_line_number, biggest_line = max(enumerate(fin), key=tuple_compare)
    print(biggest_line_number, biggest_line, len(biggest_line))