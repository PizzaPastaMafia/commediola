#Nel codice s'apre il testo oscuro,
#Commedia.txt tra errori dispersi,
#Svelando il senso, un'opera di puro.

#Il programma, vigil, corregge i rovesci,
#Risplende il testo, l'opera rinasce,
#Ombre fugaci, luce nei versi.

import re

def clean_text(input_text):
    # Remove numbers
    text_no_numbers = re.sub(r'\d+', '', input_text)
    # Remove text within square brackets
    text_no_square_brackets = re.sub(r'\[.*?\]', '', text_no_numbers)
    # Remove square brackets
    clean_text = text_no_square_brackets.replace('[', '').replace(']', '')
    #clean_text = clean_text.replace('\n\n\n', '[c]').replace('\n\n', '[t]').replace('\n', '[v]')
    return clean_text

def main():
    # Input and output file paths
    input_file = 'commedia.txt'
    output_file = "commedia1.txt"
    
    # Read data from input file
    with open(input_file, 'r', encoding="utf8") as file:
        data = file.read()

    # Clean the text
    cleaned_data = clean_text(data)

    # Write cleaned data to output file
    with open(output_file, 'w', encoding="utf8") as file:
        file.write(cleaned_data)

    print("Text cleaned and saved to", output_file)

if __name__ == "__main__":
    main()