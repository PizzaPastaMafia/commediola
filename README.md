# commediola



## How much can you automate layout design?

surely I won't copy the entire Divina Commedia by hand, so I'm using this project to learn better about extendscript

## how it works
commedia.txt (integral text i fond online) -> dante.py-> commedia1.txt (clean text) <br>
commedia1.txt -> virgilio.py -> divina_commedia.json<br>
divina_commedia.json -> source1.jsx (where the magic happens) -> commedia indesign packet

## how about the actual design?
the layout will be very simple and clean, I'd say _swiss_.<br>
firstly I though of one of those narrow and tall books that now are so in vogue but I realized it didn't make much sense, the text would be so crumped up. <br>
So I decided: each page a terzina, and it will be as wide as the longest verse so each verse will have all the space it needs without breakpoints.

## todo
the source1.jsx for now just parses the json but for each terzina it must create a page, create the texboxes and write each verse
also title page for each canto and each section

> fatti non foste a viver come bruti, <br>
> ma per seguir virtute e canoscenza
