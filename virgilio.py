#Nel codice, s'apre il testo, Commedia(1.txt),
#Tra le righe antiche, la parola è ambita,
#Lo scritto, un viaggio, per la vita eterna.

#Il programma vigile, in una danza ardita,
#Trasforma il testo in struttura di senso,
#Nel file JSON, l'opera si invita.

import re
import json

# Read the raw text file
with open('commedia1.txt', 'r', encoding='utf-8') as file:
    raw_text = file.read()

# Split the text into sections based on the titles like "INFERNO", "PURGATORIO", "PARADISO"
sections = re.split(r'\b(INFERNO|PURGATORIO|PARADISO)\b', raw_text)
sections = [section.strip() for section in sections if section.strip()]

# Initialize the JSON structure
json_structure = {
    "title": "Divina Commedia",
    "author": "Dante Alighieri",
    "sections": []
}

section_title_map = {"INFERNO": "Inferno", "PURGATORIO": "Purgatorio", "PARADISO": "Paradiso"}
terzinecnt = 0
# Process each section
for i in range(0, len(sections), 2):
    section_title = sections[i]
    section_content = sections[i + 1]

    section_object = {
        "title": section_title_map[section_title],
        "cantos": []
    }

    # Find all canto titles and their content
    cantos = re.findall(r'(Canto [IVXLCDM]+)\s*\n(.*?)\s*(?=(Canto [IVXLCDM]+|$))', section_content, re.DOTALL)

    # Process each canto
    for canto_content in cantos:

        print(canto_content[0])

        canto_object = {
            "title": canto_content[0],
            "terzine": []
        }

        cc = canto_content[1].split('\n')

        print(cc)

        terzina = []
        for line in cc:
            line = line.strip()
            if line:
                terzina.append(line)
                if len(terzina) == 3:
                    canto_object["terzine"].append(terzina.copy())
                    terzinecnt+=1
                    terzina.clear()
            elif terzina:
                canto_object["terzine"].append(terzina.copy())
                terzinecnt+=1
                terzina.clear()
                
        if terzina:
            canto_object["terzine"].append(terzina.copy())
            terzinecnt+=1
            terzina.clear()

        section_object["cantos"].append(canto_object)
        print(terzinecnt)

    json_structure["sections"].append(section_object)

# Write the JSON structure to a file
with open('divina_commedia.json', 'w', encoding='utf-8') as json_file:
    json.dump(json_structure, json_file, ensure_ascii=False, indent=2)

print('Conversion complete. JSON file created.')